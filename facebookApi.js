
var FB = require('fb');
var constants = require('./const');

module.exports = {
  getCheckins : function(pageId, callback) {
    FB.api('/' + constants[pageId], {
       fields:         'checkins',
       access_token:   process.env.FB_ACCESS_TOKEN
    }, function (result) {
      //console.log("[FB][CHECKINS] Getting no. of checkins for " + pageId + " : ", result);
      callback(result.checkins);
    });
  }
}

