// server.js
// where your node app starts

// init project
var express = require('express');
var app = express();
var env = require('node-env-file');
env('./.env');
var mongodb = require('mongodb');
var facebookApi = require('./facebookApi');
var uri = 'mongodb://'+process.env.DB_USER+':'+process.env.DB_PASS+'@'+process.env.DB_HOST+':'+process.env.DB_PORT+'/'+process.env.DB_NAME;

var foursquareApi = require('./api/foursquare');

app.use(express.static('public'));

app.get("/", function (request, response)  {
	console.log("/ req");
  	response.sendFile(__dirname + '/views/index.html');
});

app.get("/facebookApi/checkins", function (request, response) {
  console.log("[SERVER][facebookApi] Request " + request.query.pageId);
  facebookApi.getCheckins(request.query.pageId, function(facebookResponse){
    response.json(facebookResponse);
  });
});


// listen for requests :)
var listener = app.listen(process.env.APP_PORT, function () {
  console.log('[SERVER] Your app is listening on port ' + listener.address().port);
});

mongodb.MongoClient.connect(uri, function(err, db) {
	if(err) throw err;
	console.log("[SERVER] Connected to db.");
	var dbCheckins = db.collection('checkins');
	//var lastCheckinsNo = 0;
	var interval = setInterval(function() {
		facebookApi.getCheckins("FRATELLI_PAGE_ID", function(facebookCheckins){
			foursquareApi.getCheckins(function(foursquareCheckins) {
				console.log("foursquareCheckins", foursquareCheckins);
				var checkin = {
				 	 	date: Date.now(),
				 	 	facebook: facebookCheckins,
				 	 	foursquare: foursquareCheckins,
				 	 	total: foursquareCheckins + facebookCheckins
				}
				dbCheckins.insert(checkin);
				console.log("Crawler -> ", checkin);
				// if (lastCheckinsNo != facebookCheckins) {
				// 	var checkin = {
				//  	 	date: Date.now(),
				//  	 	noOfCheckins: facebookCheckins
				// 	}
				// 	dbCheckins.insert(checkin);
				// }
				// lastCheckinsNo = facebookCheckins;
			});			
		});
	}, 600000); //600000
});
