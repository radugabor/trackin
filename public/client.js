// client-side js
// run by the browser each time your view template is loaded

// by default, you've got jQuery,
// add other scripts at the bottom of index.html

$(function() {
  console.log('Client initialized :)');

  $.get( "/facebookApi/checkins", { pageId: "FRATELLI_PAGE_ID"} )
  .done(function( data ) {
    $("#fratelli-checkins").text(data);
  });
});
